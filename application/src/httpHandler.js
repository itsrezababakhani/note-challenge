import axios from "axios";
import { Component } from "react";
export default class HttpHandler extends Component {
  constructor(props) {
    super(props);
    this.preUrl = "https://challenge.ronash.co/react";
  }
  get(url, options) {
    return axios.get(`${this.preUrl}${url}`, options);
  }

  delete(url, options) {
    return axios.delete(`${this.preUrl}${url}`, options);
  }

  post(url, params, options) {
    return axios.post(`${this.preUrl}${url}`, params, options);
  }
}
