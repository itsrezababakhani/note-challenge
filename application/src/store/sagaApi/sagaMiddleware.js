import { put, all, takeEvery, call } from "redux-saga/effects";
import actions from "../actions";
import httpHandler from "../../httpHandler";
const http = new httpHandler();
function* initAppWorker(action) {
  try {
    const token = localStorage.getItem("jwt");
    if (token) {
      yield put({
        type: actions.INIT_APP_SUCCESS,
        payload: { isUserLoggedIn: true, token }
      });
    }
  } catch (error) {
    yield put({
      type: actions.INIT_APP_FAILED,
      payload: {
        hasError: true,
        errorMessage: "there is something wrong try again"
      }
    });
  }
}

function* signWorker(action) {
  try {
    const signRequest = yield call(async () => {
      return await http.post("/api/v1/auth/token", action.payload);
    });
    yield localStorage.setItem("jwt", signRequest.data.token);
    yield put({
      type: actions.SIGN_SUCCESS,
      payload: { isUserLoggedIn: true, token: signRequest.data.token }
    });

    {
      document.location.reload();
    }
  } catch (error) {
    yield put({
      type: actions.SIGN_FAILED,
      payload: { hasError: true, errorMessage: error.response.data }
    });
  }
}

function* savePostWorker(action) {
  const token = localStorage.getItem("jwt");
  if (token) {
    try {
      const postRequest = yield call(async () => {
        return await http.post("/api/v1/notes", action.payload, {
          headers: { Authorization: `jwt ${token}` }
        });
      });
      const { _id } = postRequest.data;
      const payloadWithID = { ...action.payload, _id };
      yield put({
        type: actions.SAVE_POST_SUCCESS,
        payload: payloadWithID
      });
    } catch (error) {
      return put({
        type: actions.SAVE_POST_FAILED,
        payload: { hasError: true, errorMessage: "error in save request" }
      });
    }
  }
}

function* getPostsWorker(action) {
  const token = localStorage.getItem("jwt");
  if (token) {
    try {
      const getPosts = yield call(async () => {
        return await http.get("/api/v1/notes", {
          headers: { Authorization: `jwt ${token}` }
        });
      });

      yield put({
        type: actions.GET_POST_SUCCESS,
        payload: getPosts.data
      });
    } catch (error) {
      return put({
        type: actions.GET_POST_FAILED,
        payload: { hasError: true, errorMessage: "error in get request" }
      });
    }
  }
}
function* deletePostWorker(action) {
  const token = localStorage.getItem("jwt");
  if (token) {
    try {
      yield call(async () => {
        return await http.delete(`/api/v1/notes/${action.payload}`, {
          headers: { Authorization: `jwt ${token}` }
        });
      });

      yield put({
        type: actions.DELETE_POST_SUCCESS,
        payload: action.payload
      });
    } catch (error) {
      return put({
        type: actions.DELETE_POST_FAILED,
        payload: { hasError: true, errorMessage: "error in delete request" }
      });
    }
  }
}

function* deletePostWatcher() {
  yield takeEvery(actions.DELETE_POST, deletePostWorker);
}

function* getPostsWatcher() {
  yield takeEvery(actions.GET_POST, getPostsWorker);
}

function* savePostWatcher() {
  yield takeEvery(actions.SAVE_POST, savePostWorker);
}

function* initAppWatcher() {
  yield takeEvery(actions.INIT_APP, initAppWorker);
}
function* signWatcher() {
  yield takeEvery(actions.DO_SIGN, signWorker);
}
export default function* mySaga() {
  yield all([
    initAppWatcher(),
    signWatcher(),
    savePostWatcher(),
    getPostsWatcher(),
    deletePostWatcher()
  ]);
}
