import actions from "./actions";
const initState = {
  isUserLoggedIn: false,
  dialogBox: false,
  dialogPostTitle: null,
  posts: [],
  hasError: null,
  token: null
};
export default function initApplication(state = initState, action) {
  let newState = state;
  switch (action.type) {
    case actions.DELETE_POST_SUCCESS:
      newState = {
        ...state,
        posts: state.posts.filter(post => {
          return post._id !== action.payload;
        })
      };
      break;
    case actions.DELETE_POST_FAILED:
      newState = { ...state, ...action.payload };
      break;
    case actions.SAVE_POST_SUCCESS:
      newState = {
        ...state,
        posts: [...state.posts, action.payload],
        dialogBox: false
      };
      break;
    case actions.GET_POST_SUCCESS:
      newState = { ...state, posts: [...state.posts, ...action.payload] };
      break;
    case actions.DISCARD_POST:
      newState = { ...state, dialogBox: false, dialogPostTitle: null };
      break;
    case actions.OPEN_DIALOG_POST_BOX:
      newState = { ...state, dialogBox: true, dialogPostTitle: action.payload };
      break;
    case actions.INIT_APP_FAILED:
      newState = { ...state, ...action.payload };
      break;
    case actions.INIT_APP_SUCCESS:
      newState = { ...state, ...action.payload };
      break;
    case actions.SIGN_SUCCESS:
      newState = { ...state, ...action.payload };
      break;
    case actions.SIGN_FAILED:
      newState = { ...state, ...action.payload };
      break;
    default:
      break;
  }
  return newState;
}
