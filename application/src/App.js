import React, { useState } from "react";
import { connect } from "react-redux";
import Header from "./components/headers/Header";
import Container from "@material-ui/core/Container";
import PostMinBox from "./components/forms/PostMinBox";
import PostDialogBox from "./components/dialogs/PostDialogBox";
import PostCard from "./components/cards/PostCard";
import AuthDialog from "./components/dialogs/AuthDialog";
import actions from "./store/actions";
import PaperAlert from "./components/alerts/PaperAlert";

function App({ dialogBoxState, userLoggedIn, posts, getPosts }) {
  const [showDialog, setShowDialog] = useState(false);
  const dialogBoxHandler = visiblity => {
    setShowDialog(visiblity);
  };
  const renderPostCard = posts.map(post => {
    return <PostCard {...post} key={post._id} />;
  });

  return (
    <div className="App">
      <Header onShowDialog={dialogBoxHandler} />
      <Container maxWidth="sm">
        <PostMinBox />
        {dialogBoxState && <PostDialogBox />}
        {!userLoggedIn && (
          <PaperAlert message="The user require to log in to display posts" />
        )}
        {renderPostCard.length < 1 && userLoggedIn ? (
          <PaperAlert message="there is no card to showing" />
        ) : (
          renderPostCard
        )}

        <AuthDialog
          onCloseDialog={dialogBoxHandler}
          dialogVisibility={showDialog}
        />
      </Container>
    </div>
  );
}

export default connect(
  state => {
    return {
      dialogBoxState: state.dialogBox,
      userLoggedIn: state.isUserLoggedIn,
      posts: state.posts
    };
  },
  dispatch => {
    return {
      getPosts: payload => {
        dispatch({
          type: actions.GET_POST,
          payload
        });
      }
    };
  }
)(App);
