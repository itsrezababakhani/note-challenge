import React from "react";
import actions from "../../store/actions";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import postMinBoxStyle from "../../styles/components/forms/postMinBox";
const style = makeStyles(postMinBoxStyle);
function PostMinBox({ openDialogBox }) {
  const classes = style();
  const openDialogBoxHandler = e => {
    const titleTextField = document.querySelector("#titleField").value;
    openDialogBox(titleTextField);
  };
  return (
    <>
      <TextField
        className={classes.textField}
        label="take a note ..."
        variant="outlined"
        id="titleField"
      />
      <Button
        onClick={e => {
          openDialogBoxHandler(e);
        }}
        className={classes.button}
        variant="contained"
        color="primary"
      >
        set post title
      </Button>
    </>
  );
}
export default connect(null, dispatch => {
  return {
    openDialogBox: payload => {
      dispatch({
        type: actions.OPEN_DIALOG_POST_BOX,
        payload
      });
    }
  };
})(PostMinBox);
