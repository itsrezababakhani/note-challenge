import React from "react";
import { Paper, Typography, makeStyles } from "@material-ui/core";
import paperAlertStyle from "../../styles/components/alerts/paperAlert";

const style = makeStyles(paperAlertStyle);
export default function PaperAlert({ message }) {
  const classes = style();
  return (
    <Paper className={classes.alertBox}>
      <Typography variant="h6" component="h6">
        {message}
      </Typography>
    </Paper>
  );
}
