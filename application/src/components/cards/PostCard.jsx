import React, { useState } from "react";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import postCardStyle from "../../styles/components/cards/postCard";
import { makeStyles } from "@material-ui/core/styles";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import IconButton from "@material-ui/core/IconButton";
import { MenuItem, Paper, MenuList } from "@material-ui/core";
import { connect } from "react-redux";
import actions from "../../store/actions";

const style = makeStyles(postCardStyle);
function PostCard({ title, content, _id, deleteHandler }) {
  const [menu, setMenu] = useState(false);
  const classes = style();
  return (
    <div>
      <Card className={classes.card}>
        <div className={classes.contentSection}>
          <div className={classes.title}>{title}</div>
          <div className={classes.description}>{content}</div>
        </div>
        <CardActions className={classes.cardActionArea}>
          <IconButton
            className={classes.cardIcon}
            onClick={e => {
              setMenu(menu ? false : true);
            }}
          >
            <MoreVertIcon />
          </IconButton>
        </CardActions>
      </Card>
      {menu ? (
        <Paper>
          <MenuList
            onClick={() => {
              console.log(_id);
              deleteHandler(_id);
            }}
          >
            <MenuItem>Delete</MenuItem>
          </MenuList>
        </Paper>
      ) : null}
    </div>
  );
}
export default connect(null, dispatch => {
  return {
    deleteHandler: payload => {
      return dispatch({
        type: actions.DELETE_POST,
        payload
      });
    }
  };
})(PostCard);
