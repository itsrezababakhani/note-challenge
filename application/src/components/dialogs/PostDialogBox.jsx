import React, { useState } from "react";
import { connect } from "react-redux";
import actions from "../../store/actions";
import {
  DialogActions,
  Button,
  Dialog,
  DialogContent,
  TextField,
  makeStyles
} from "@material-ui/core";
import postDialogBoxStyle from "../../styles/components/dialogs/postDialogBox";
const style = makeStyles(postDialogBoxStyle);
function PostDialogBox({ savePostHandler, discardHandler, dialogPostTitle }) {
  const [titleValidateError, setTitleValidateError] = useState(null);
  const [contentValidateError, setContentValidateError] = useState(null);
  const classes = style();
  const onSaveHandler = e => {
    const title = document.querySelector("#postTitle").value;
    const content = document.querySelector("#postDescription").value;
    if (title.length <= 5) {
      return setTitleValidateError(true);
    }
    if (content.length < 1) {
      return setContentValidateError(true);
    }
    return savePostHandler({ title, content });
  };

  return (
    <Dialog className={classes.dialogBox} open={true}>
      <DialogContent>
        <TextField
          className={classes.textInput}
          id="postTitle"
          label="title ..."
          variant="outlined"
        />
        {titleValidateError && (
          <span className={classes.errorSpan}>title must at least 5</span>
        )}
        <TextField
          value={dialogPostTitle}
          className={classes.textInput}
          multiline
          id="postDescription"
          rows="5"
          label="take a note ..."
          variant="outlined"
        />
        {contentValidateError && (
          <span className={classes.errorSpan}>content cant be empty</span>
        )}
      </DialogContent>
      <DialogActions>
        <Button
          onClick={e => {
            discardHandler();
          }}
          color="secondary"
        >
          discard
        </Button>
        <Button
          onClick={e => {
            onSaveHandler(e);
          }}
          color="primary"
          autoFocus
        >
          save
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default connect(
  state => {
    return {
      dialogPostTitle: state.dialogPostTitle
    };
  },
  dispatch => {
    return {
      savePostHandler: payload => {
        dispatch({
          type: actions.SAVE_POST,
          payload
        });
      },
      discardHandler: payload => {
        dispatch({
          type: actions.DISCARD_POST,
          payload
        });
      }
    };
  }
)(PostDialogBox);
