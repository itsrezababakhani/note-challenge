import React, { useState } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import authDialogStyle from "../../styles/components/dialogs/authDialog";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import { Button } from "@material-ui/core";
import { connect } from "react-redux";
import actions from "../../store/actions";
import validator from "email-validator";
const style = makeStyles(authDialogStyle);
function AuthDialog({ doSign, onCloseDialog, dialogVisibility, currentState }) {
  const [emailError, setEmailError] = useState(null);
  const classes = style();
  return (
    <Dialog className={classes.dialogBox} open={dialogVisibility}>
      <DialogTitle className={classes.dialogBoxTitle}>
        <IconButton
          onClick={() => {
            onCloseDialog(false);
          }}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>

      <TextField
        className={classes.authInput}
        id="email"
        type="email"
        label="enter email"
        variant="outlined"
      />
      {emailError && (
        <span className={classes.alertSpan}>email is not valid</span>
      )}
      <TextField
        className={classes.authInput}
        id="password"
        type="password"
        label="password"
        variant="outlined"
      />
      {currentState.hasError && (
        <span className={classes.alertSpan}>{currentState.errorMessage}</span>
      )}
      <DialogActions>
        <Button
          className={classes.authButton}
          variant="contained"
          color="secondary"
          onClick={e => {
            const email = document.querySelector("#email").value;
            const password = document.querySelector("#password").value;
            const userData = { email, password };
            if (!validator.validate(email)) {
              setEmailError(true);
            }
            doSign(userData);
          }}
        >
          Login or register
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default connect(
  state => {
    return {
      currentState: state
    };
  },
  dispatch => {
    return {
      doSign: payload => {
        dispatch({
          type: actions.DO_SIGN,
          payload
        });
      }
    };
  }
)(AuthDialog);
