import React from "react";
import AppBar from "@material-ui/core/AppBar";
import { makeStyles } from "@material-ui/core/styles";
import headerStyle from "../../styles/components/headers/header";
import { connect } from "react-redux";
import { Button, Avatar } from "@material-ui/core";
const style = makeStyles(headerStyle);
function Header({ currentState, onShowDialog }) {
  const classes = style();
  return (
    <AppBar className={classes.header}>
      {currentState.isUserLoggedIn ? (
        <Avatar className={classes.avatar}>PU</Avatar>
      ) : (
        <Button
          className={classes.signButton}
          variant="contained"
          color="primary"
          onClick={() => {
            onShowDialog(true);
          }}
        >
          login/register
        </Button>
      )}
    </AppBar>
  );
}
export default connect(state => {
  return {
    currentState: state
  };
})(Header);
