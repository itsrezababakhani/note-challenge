const postCardStyle = () => ({
  title: {
    fontSize: 20
  },
  description: {
    fontSize: 14,
    marginTop: 10
  },
  contentSection: {
    padding: 20
  },
  cardActionArea: {
    justifyContent: "flex-end",
    borderTop: "1px solid #c3c3c3"
  },
  showMenu: {
    display: "flex"
  },
  hideMenu: {
    display: "none"
  },
  cardIcon: {
    color: "#fff"
  },

  card: {
    backgroundImage: "linear-gradient(19deg, #21D4FD 0%, #B721FF 100%)",
    color: "#000",
    padding: 10,
    margin: "30px 0px"
  }
});
export default postCardStyle;
