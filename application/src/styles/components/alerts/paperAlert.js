const paperAlertStyle = () => ({
  alertBox: {
    marginTop: 50,
    padding: "30px 15px",
    background: "#e74c3c",
    color: "white"
  }
});
export default paperAlertStyle;
