const dialogBoxStyle = () => ({
  dialogBox: {
    "& .MuiPaper-root": {
      minWidth: 600
    }
  },
  textInput: {
    minWidth: "100%",
    marginTop: 15
  },
  errorSpan: {
    color: "#e74c3c",
    lineHeight: 2.5
  }
});
export default dialogBoxStyle;
