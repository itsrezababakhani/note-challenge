const authDialogStyle = () => ({
  dialogBox: {
    "& .MuiPaper-root": {
      minWidth: 300,
      padding: "0px 20px"
    }
  },
  authInput: {
    marginTop: 20
  },
  authButton: {
    height: 50,
    width: "100%",
    marginTop: 20
  },
  dialogBoxTitle: {
    textAlign: "center"
  },
  alertSpan: {
    color: "#e74c3c"
  }
});
export default authDialogStyle;
