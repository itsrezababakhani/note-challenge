import { primaryGradient } from "../../variable";
const postMinStyle = () => ({
  textField: {
    width: "100%",
    marginTop: 40
  },
  button: {
    margin: "0 auto",
    display: "block",
    width: 200,
    height: 50,
    background: primaryGradient,
    marginTop: 20
  }
});
export default postMinStyle;
