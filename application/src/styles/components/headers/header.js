import { primaryGradient, primaryColor } from "../../variable";
const headerStyle = () => ({
  header: {
    minHeight: 60,
    position: "relative",
    background: primaryGradient
  },
  signButton: {
    width: 200,
    marginTop: 10,
    marginLeft: 20
  },
  avatar: {
    marginTop: 10,
    marginLeft: 20,
    background: primaryColor
  }
});
export default headerStyle;
