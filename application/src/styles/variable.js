const primaryColor = "#007BEC";
const primaryGradient = "linear-gradient(30deg, #22a9e0 10%, #70bf58 100%)";
export { primaryColor, primaryGradient };
