import React, { useEffect } from "react";
import { connect } from "react-redux";
import actions from "./store/actions";
import App from "./App";
function InitApp({ initCheckApp, currentState, getPosts }) {
  useEffect(() => {
    return initCheckApp(null);
  }, []);

  useEffect(() => {
    return getPosts(null);
  }, []);

  return <App />;
}

export default connect(
  state => {
    return {
      currentState: state
    };
  },
  dispatch => {
    return {
      initCheckApp: payload => {
        dispatch({
          type: actions.INIT_APP,
          payload
        });
      },
      getPosts: payload => {
        dispatch({
          type: actions.GET_POST,
          payload
        });
      }
    };
  }
)(InitApp);
